locals {
  files = fileset(var.index_templates_dir, "*.json")
}

resource "opensearch_composable_index_template" "index_template" {
  for_each = { for file in local.files : trimsuffix(file, ".json") => file }

  name = each.key
  body = file("${var.index_templates_dir}/${each.value}")
}
