variable "index_templates_dir" {
  description = <<EOT
  Path to dir containing index_templates definintions in .json files

  Example dir structure:
  index_templates/
  ├── logs-default.json
  └── logs-nginx_access.json

  Name of a file is used as a name of index_template

  EOT
  type        = string
  default     = "./index_templates"
}
