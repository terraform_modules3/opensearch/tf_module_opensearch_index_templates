# Terraform Opensearch Index Templates Module
Creates Opensearch index templates

## Usage example
```hcl
# main.tf
module "index_template" {
  source = "git::https://gitlab.com/terraform_modules3/opensearch/tf_module_opensearch_index_templates.git?ref=0.1.0"
}
```
```
.
├── main.tf
└── index_templates
    ├── logs-default.json
    └── logs-nginx_access.json
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.1 |
| <a name="requirement_opensearch"></a> [opensearch](#requirement\_opensearch) | >= 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_opensearch"></a> [opensearch](#provider\_opensearch) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [opensearch_composable_index_template.index_template](https://registry.terraform.io/providers/opensearch-project/opensearch/latest/docs/resources/composable_index_template) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_index_templates_dir"></a> [index\_templates\_dir](#input\_index\_templates\_dir) | Path to dir containing index\_templates definintions in .json files<br><br>  Example dir structure:<br>  index\_templates/<br>  ├── logs-default.json<br>  └── logs-nginx\_access.json<br><br>  Name of a file is used as a name of index\_template | `string` | `"./index_templates"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
