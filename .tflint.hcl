# https://github.com/terraform-linters/tflint/blob/master/docs/user-guide/config.md

config {
  format = "default"
}

# https://github.com/terraform-linters/tflint-ruleset-terraform/tree/main/docs/rules
rule "terraform_unused_required_providers" {
  enabled = true
}
rule "terraform_documented_outputs" {
  enabled = true
}
rule "terraform_documented_variables" {
  enabled = true
}
rule "terraform_standard_module_structure" {
  enabled = true
}
